# Для запуска
Изначально необходимо создать .env файл и заролнить его как указано в .env.example <br>
Далее:
```bash
yarn install
yarn start:dev
```
Если не указывать AUTO_RUN_MIGRATION=ture, то небоходимо запустить команду

```bash
yarn migration:run
```
После необходимо подождать какое-то время чтобы парсер собрал какое-то кол-во значений, за одну итерацию он собирает 2000 блоков (если указывать больше можно уткнуться в ограничение по запросам)

### Как происходит поиск кошелька по требования тз

```sql
WITH blocks AS (
	select "blockNumber" from "transaction" GROUP BY "blockNumber" ORDER BY "blockNumber" DESC LIMIT 100
)
SELECT *, (v1.val - COALESCE(v2.val, 0)) new_val FROM (
	SELECT "to" as hash, SUM(value) val
	FROM "transaction" WHERE "to" IS NOT NULL AND "blockNumber" IN (SELECT * FROM blocks)
	GROUP bY "to"
) v1 LEFT JOIN (
	SELECT "from" hash, SUM(value) val
	FROM "transaction" WHERE "from" IS NOT NULL AND "blockNumber" IN (SELECT * FROM blocks)
	GROUP bY "from"
) v2 ON (v1.hash = v2.hash)
ORDER BY new_val DESC
LIMIT 1
```
