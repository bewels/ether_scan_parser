module.exports = {
  extends: [
    'airbnb-base',
    'airbnb-typescript/base',
    'plugin:@typescript-eslint/recommended',
    'plugin:promise/recommended',
    'plugin:sonarjs/recommended',
		'plugin:prettier/recommended',
    'prettier',
  ],
  parserOptions: {
    project: 'tsconfig.json',
		tsconfigRootDir: __dirname
  },
  env: {
    node: true,
    es2021: true
  },
  rules: {
    'linebreak-style': 0,
    'class-methods-use-this': 'off',

    'sonarjs/no-small-switch': 'off',
    'sonarjs/no-duplicate-string': 'error',
    'sonarjs/no-duplicate-string': ['error', 10],

    'no-plusplus': 'off',

    'max-len': ['error', { code: 8000 }],

    'max-classes-per-file': ['error', 6],

    '@typescript-eslint/indent': 'off',
    '@typescript-eslint/return-await': 'off',

    'arrow-parens': [
      2,
      'as-needed',
      {
        requireForBlockBody: true,
      },
    ],

    'padding-line-between-statements': [
      'error',
      {
        blankLine: 'always',
        prev: '*',
        next: 'return',
      },
    ],
    'import/prefer-default-export': 'off',
    'import/no-default-export': 'warn',
    'import/no-absolute-path': 'error',
    'import/no-extraneous-dependencies': ['error', { devDependencies: true }],

    'object-curly-newline': [
      'error',
      {
        ImportDeclaration: {
          multiline: true,
          minProperties: 12,
        },
        ExportDeclaration: {
          multiline: true,
          minProperties: 12,
        },
      },
    ],

    'no-underscore-dangle': [
      'error',
      {
        allowAfterThis: true,
      },
    ],

    'import/order': [
      'error',
      {
        'newlines-between': 'always',
        groups: [
          ['builtin', 'external'],
          ['internal', 'unknown', 'parent', 'sibling', 'index', 'object', 'type'],
        ],
        pathGroups: [
          { pattern: '@core/**', group: 'internal' },
          { pattern: '@features/**', group: 'internal' },
          { pattern: '@infrastructure/**', group: 'internal' },
        ],
        pathGroupsExcludedImportTypes: ['internal'],
      },
    ],

    'generator-star-spacing': ['error', 'both'],

    '@typescript-eslint/naming-convention': [
      'error',
      {
        selector: 'variable',
        modifiers: ['destructured'],
        format: null,
      },
    ],

    'operator-linebreak': 'off',
    'arrow-parens': 'off',
    'prefer-destructuring': 'off',
    'new-cap': 'off',
    "sonarjs/cognitive-complexity": 'off',
    "spaced-comment": ["error", "always", { "exceptions": ["TODO", "FIXME"] }],
    'no-param-reassign': 'off',
    'no-restricted-syntax': 'off',
    'guard-for-in': 'off',
    'no-await-in-loop': 'off',
    'no-continue': 'off',
    'no-useless-return': 'off',
    'no-nested-ternary': 'off',
    'no-case-declarations': 'off',
    'import/no-cycle': 'off',
    'no-unused-vars': ["error", { "varsIgnorePattern": "_" }],
    '@typescript-eslint/no-unused-vars': ['error', { "varsIgnorePattern": "_*" }],
    '@typescript-eslint/no-unused-expressions': 'warn',
    '@typescript-eslint/no-var-requires': 'off',
    "@typescript-eslint/no-useless-constructor": "warn",
    'sonarjs/no-duplicate-string': 'off',
    'sonarjs/no-nested-template-literals': 'off',
    'prefer-rest-params': 'off',
    'import/no-dynamic-require': 'off',
    'global-require': 'off',
    'no-control-regex': 'off',
    'no-underscore-dangle': 'off',
    'no-prototype-builtins': 'off',
    'import/no-default-export': 'off',
    'import/no-extraneous-dependencies': 'off',
    '@typescript-eslint/no-throw-literal': 'off',
    '@typescript-eslint/ban-types': 'off',
    '@typescript-eslint/no-loop-func': 'off',
		'@typescript-eslint/ban-ts-comment': 'off',
		'@typescript-eslint/no-redeclare': 'off',
		'no-labels': 'off',
		'generator-star-spacing': 'off'
	},
};
