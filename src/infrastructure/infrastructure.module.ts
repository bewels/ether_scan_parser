import { Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';

import { DatabaseModule } from './database';

@Module({
	imports: [DatabaseModule, ScheduleModule.forRoot()],
	providers: [],
})
export class InfrastructureModule {}
