import { Module } from '@nestjs/common';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { join } from 'path';
import { DataSource, DataSourceOptions } from 'typeorm';

import env from '@core/config/env';

const postgresOptions: TypeOrmModuleOptions & DataSourceOptions = {
	type: 'postgres' as const,
	database: env.DB_NAME,
	username: env.DB_USER,
	password: env.DB_PASS,
	port: env.DB_PORT,
	host: env.DB_HOST,
	migrationsRun: env.AUTO_RUN_MIGRATION,
	entities: [join(__dirname, '../../entities/**/*.entity.{ts,js}')],
	migrations: [join(__dirname, '../../migrations/**/*.{ts,js}')],
};

@Module({
	imports: [TypeOrmModule.forRoot(postgresOptions)],
})
export class DatabaseModule {}

// FOR DATABASE TOOLS
export default new DataSource(postgresOptions);
