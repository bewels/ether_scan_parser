import { Module } from '@nestjs/common';

import { InfrastructureModule } from '@infrastructure/infrastructure.module';
import { FeaturesModule } from '@features/features.module';

@Module({
	imports: [InfrastructureModule, FeaturesModule],
	providers: [],
})
export class AppModule {}
