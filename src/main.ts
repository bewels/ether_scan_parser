import { NestFactory } from '@nestjs/core';

import { AppModule } from './app.module';
import env from './core/config/env';

async function bootstrap() {
	const app = await NestFactory.create(AppModule, {
		cors: true,
	});

	app.setGlobalPrefix('api');

	await app.listen(env.PORT);
}
bootstrap();
