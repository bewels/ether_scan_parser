import { TransactionEntity } from '@entities/transaction.entity';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectDataSource, InjectRepository } from '@nestjs/typeorm';
import { DataSource, Repository } from 'typeorm';

import { IGetMaxForLastHundredBlocks } from './types/GetMaxForLastHundredBlocks.type';

@Injectable()
export class BalanceService {
	constructor(
		@InjectRepository(TransactionEntity)
		private readonly transactionRepo: Repository<TransactionEntity>,
		@InjectDataSource()
		private readonly dataSource: DataSource,
	) {}

	async maxForLastHundredBlocks(): Promise<IGetMaxForLastHundredBlocks> {
		// cte can also be used, but controllability and typesafety are lost
		const blockNumbers = await this.transactionRepo
			.createQueryBuilder('trz')
			.select('"blockNumber" "blockNumber"')
			.orderBy('trz.blockNumber', 'DESC')
			.groupBy('trz.blockNumber')
			.take(100)
			.getRawMany()
			.then((res) => res.map((item) => item.blockNumber));

		const fromQuery = this.dataSource
			.createQueryBuilder()
			.from('transaction', 'trz')
			.select('"from" hash, SUM(value) val')
			.where('"from" IS NOT NULL')
			.andWhere('"blockNumber" IN (:...blockNumbers)')
			.groupBy('"from"');

		const result = await this.dataSource
			.createQueryBuilder()
			.select('(v1.val - COALESCE(v2.val, 0)) new_val, v1.hash')
			.from((qb) => {
				return qb
					.select('"to" hash, SUM(value) val')
					.from('transaction', 'trz')
					.where('"to" IS NOT NULL')
					.andWhere('"blockNumber" IN (:...blockNumbers)')
					.groupBy('"to"');
			}, 'v1')
			.leftJoinAndSelect(`(${fromQuery.getQuery()})`, 'v2', '(v1.hash = v2.hash)')
			.orderBy('new_val', 'DESC')
			.setParameter('blockNumbers', blockNumbers)
			.getRawOne<IGetMaxForLastHundredBlocks>();

		if (!result) throw new NotFoundException('Wallet not found!');

		return result;
	}
}
