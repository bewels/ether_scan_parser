import { IGetMaxForLastHundredBlocks } from '@features/balance/types/GetMaxForLastHundredBlocks.type';

export class GetMaxForLastHundredBlocksDto {
	hash: string;

	constructor(result: IGetMaxForLastHundredBlocks) {
		this.hash = result.hash;
	}
}
