import { Controller, Get } from '@nestjs/common';

import { GetMaxForLastHundredBlocksDto } from './dto/response/GetMaxForLastHundredBlocks.dto';
import { BalanceService } from './balance.service';

@Controller('balance')
export class BalanceController {
	constructor(private readonly balanceService: BalanceService) {}

	@Get('max-for-last-hundred-blocks')
	async maxForLastHundredBlocks(): Promise<GetMaxForLastHundredBlocksDto> {
		const result = await this.balanceService.maxForLastHundredBlocks();

		return new GetMaxForLastHundredBlocksDto(result);
	}
}
