import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TransactionEntity } from '@entities/transaction.entity';

import { BalanceController } from './balance.controller';
import { BalanceService } from './balance.service';

@Module({
	imports: [TypeOrmModule.forFeature([TransactionEntity])],
	controllers: [BalanceController],
	providers: [BalanceService],
})
export class BalanceModule {}
