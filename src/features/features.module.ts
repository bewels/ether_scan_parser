import { Module } from '@nestjs/common';

import { BalanceModule } from './balance/balance.module';
import { ParseTransactionModule } from './parse-transaction/parse-transaction.module';

@Module({
	imports: [BalanceModule, ParseTransactionModule],
})
export class FeaturesModule {}
