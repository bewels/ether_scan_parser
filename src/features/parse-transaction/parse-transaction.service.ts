import { Injectable, Logger } from '@nestjs/common';
import { Cron, SchedulerRegistry } from '@nestjs/schedule';
import { HttpService } from '@nestjs/axios';
import { Observable, catchError, forkJoin, lastValueFrom, map } from 'rxjs';
import { AxiosError, AxiosResponse } from 'axios';
import { InjectRepository } from '@nestjs/typeorm';
import { IsNull, Not, Repository } from 'typeorm';
import { TransactionEntity } from '@entities/transaction.entity';

import env from '@core/config/env';
import { IBlock } from './types/block.type';
import { IEtherscanResponse } from './types/etherscan-response.type';
import { ITransaction } from './types/transaction.type';
import { parseFloatOverride } from '@core/utils/parseFloat';

type TransactionForCreate = Omit<TransactionEntity, 'id' | 'createdAt'>;

const TRANSACTION_PARSE_SCHEDULE_NAME = 'transaction_parse';

@Injectable()
export class ParseTransactionService {
	private readonly logger = new Logger(ParseTransactionService.name);

	private readonly chunkSize = 2000;

	constructor(
		private readonly httpService: HttpService,
		@InjectRepository(TransactionEntity)
		private readonly transactionRepo: Repository<TransactionEntity>,
		private readonly schedulerRegistry: SchedulerRegistry,
	) {}

	@Cron(env.SCHEDULE_PATTERN, {
		name: TRANSACTION_PARSE_SCHEDULE_NAME,
	})
	private async _() {
		const job = this.schedulerRegistry.getCronJob(TRANSACTION_PARSE_SCHEDULE_NAME);
		job.stop();
		try {
			const lastBlockNumber = await this.getLastBlockNumber();

			const generator = this.getBlocksByLastNumber(parseInt(lastBlockNumber.result, 16));

			for await (const chunk of generator) {
				const readyTrz = chunk.map<TransactionForCreate>((item) => ({
					blockNumber: parseInt(item.blockNumber, 16),
					from: item.from,
					to: item.to,
					value: parseFloatOverride(item.value, 16),
					hash: item.hash,
				}));

				await this.transactionRepo.createQueryBuilder().insert().values(readyTrz).orIgnore(true).execute();
			}
		} catch (error) {
			this.logger.error('Parse blocks error');
		} finally {
			job.start();
		}
	}

	private async *getBlocksByLastNumber(lastEtherScanNumber: number): AsyncGenerator<ITransaction[], void, unknown> {
		let lastSavedNumber = await this.transactionRepo
			.findOne({
				where: {
					id: Not(IsNull()),
				},
				order: {
					blockNumber: 'DESC',
				},
			})
			.then((res) => {
				if (res) {
					return +res.blockNumber;
				}

				return null;
			});

		if (!lastSavedNumber) {
			lastSavedNumber = env.ETHERSCAN_START_FROM;
		}
		let counter = 0;
		let infinityLoopWork = true;
		while (infinityLoopWork) {
			const startValue = lastSavedNumber + counter;
			const requests: Observable<AxiosResponse<IEtherscanResponse<IBlock>, unknown>>[] = [];
			for (let i = startValue; i < startValue + this.chunkSize; i++) {
				if (i > lastEtherScanNumber) {
					infinityLoopWork = false;
					break;
				}

				const req = this.httpService.get<IEtherscanResponse<IBlock>>(env.ETHERSCAN_API_URL, {
					params: {
						module: 'proxy',
						action: 'eth_getBlockByNumber',
						tag: i.toString(16),
						boolean: true,
					},
				});

				requests.push(req);
			}

			const promise = new Promise<ITransaction[]>((resolve) => {
				forkJoin(requests)
					.pipe(
						catchError((error: AxiosError) => {
							this.logger.error(error);
							throw 'Get blocks error';
						}),
						map<AxiosResponse<IEtherscanResponse<IBlock>, unknown>[], ITransaction[]>((result) => {
							return result.reduce<ITransaction[]>((acc, item) => {
								const trz = item?.data?.result?.transactions;
								if (trz) {
									acc.push(...trz);
								}

								return acc;
							}, []);
						}),
					)
					.subscribe((res) => resolve(res));
			});

			counter += this.chunkSize;

			yield promise;
		}
	}

	private getLastBlockNumber(): Promise<IEtherscanResponse<string>> {
		const lastBlockNumber = this.httpService
			.get<IEtherscanResponse<string>>(env.ETHERSCAN_API_URL, {
				params: {
					module: 'proxy',
					action: 'eth_blockNumber',
				},
			})
			.pipe(
				catchError((error: AxiosError) => {
					this.logger.error(error);
					throw 'Get last block number error';
				}),
				map<AxiosResponse<IEtherscanResponse<string>, unknown>, IEtherscanResponse<string>>((result) => {
					return {
						id: result.data.id,
						jsonrpc: result.data.jsonrpc,
						result: result.data.result,
					};
				}),
			);

		return lastValueFrom(lastBlockNumber);
	}
}
