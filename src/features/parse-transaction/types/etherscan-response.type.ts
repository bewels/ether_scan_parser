export interface IEtherscanResponse<T> {
	jsonrpc: string;
	id: number;
	result: T;
}
