import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TransactionEntity } from '@entities/transaction.entity';

import { ParseTransactionService } from './parse-transaction.service';

@Module({
	imports: [HttpModule, TypeOrmModule.forFeature([TransactionEntity])],
	providers: [ParseTransactionService],
})
export class ParseTransactionModule {}
