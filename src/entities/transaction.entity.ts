import { Column, Entity } from 'typeorm';

import { BaseEntity } from './base.entity';

@Entity('transaction')
export class TransactionEntity extends BaseEntity {
	@Column({
		type: 'char',
		length: 42,
	})
	from: string;

	@Column({
		type: 'char',
		length: 66,
		unique: true,
	})
	hash: string;

	@Column({
		type: 'char',
		length: 42,
		nullable: true,
	})
	to: string;

	@Column({
		type: 'decimal',
	})
	value: number;

	@Column({
		type: 'bigint',
	})
	blockNumber: number;
}
