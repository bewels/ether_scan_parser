import { cleanEnv, str, num, bool } from 'envalid';
import { config } from 'dotenv';

config({
	debug: true,
});

const env = cleanEnv(process.env, {
	NODE_ENV: str({ default: 'development' }),
	PORT: num({ default: 3000 }),

	DB_NAME: str(),
	DB_USER: str(),
	DB_PASS: str(),
	DB_PORT: num(),
	DB_HOST: str(),

	AUTO_RUN_MIGRATION: bool({ default: false }),

	ETHERSCAN_API_URL: str(),
	SCHEDULE_PATTERN: str({ default: '* * * * *' }),
	ETHERSCAN_START_FROM: num({ default: 17583000 }),
});

export default env;
