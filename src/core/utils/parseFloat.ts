export function parseFloatOverride(str: string, radix: number) {
	const parts = str.split('.');
	if (parts.length > 1) {
		return parseInt(parts[0], radix) + parseInt(parts[1], radix) / radix ** parts[1].length;
	}

	return parseInt(parts[0], radix);
}
