import { MigrationInterface, QueryRunner } from "typeorm";

export class Initial1689883856274 implements MigrationInterface {
    name = 'Initial1689883856274'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "transaction" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "from" character(42) NOT NULL, "to" character(42) NOT NULL, "value" numeric NOT NULL, "blockNumber" bigint NOT NULL, CONSTRAINT "PK_89eadb93a89810556e1cbcd6ab9" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "transaction"`);
    }

}
