import { MigrationInterface, QueryRunner } from "typeorm";

export class SetNullableForToField1689893504606 implements MigrationInterface {
    name = 'SetNullableForToField1689893504606'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "transaction" ALTER COLUMN "to" DROP NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "transaction" ALTER COLUMN "to" SET NOT NULL`);
    }

}
