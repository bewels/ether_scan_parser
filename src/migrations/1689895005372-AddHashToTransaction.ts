import { MigrationInterface, QueryRunner } from "typeorm";

export class AddHashToTransaction1689895005372 implements MigrationInterface {
    name = 'AddHashToTransaction1689895005372'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "transaction" ADD "hash" character(66) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "transaction" ADD CONSTRAINT "UQ_de4f0899c41c688529784bc443f" UNIQUE ("hash")`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "transaction" DROP CONSTRAINT "UQ_de4f0899c41c688529784bc443f"`);
        await queryRunner.query(`ALTER TABLE "transaction" DROP COLUMN "hash"`);
    }

}
